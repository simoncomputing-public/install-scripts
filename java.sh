#yum search java | grep openjdk
sudo yum install -y java-1.8.0-openjdk-devel.x86_64
sudo update-alternatives --config java
sudo update-alternatives --config javac
java -version

echo "export JAVA_HOME=/usr/lib/jvm/ava-1.8.0-openjdk-devel.x86_64" >> ~/.bash_profile
echo Java''s home is at: $JAVA_HOME

