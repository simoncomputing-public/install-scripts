# Installation Scripts

Last updated: April 23, 2017

These are installation scripts designed for Linux installed in AWS EC2 Instances.
To use these scripts, download with these instructions.

```
wget https://gitlab.com/simoncomputing-public/install-scripts/repository/archive.zip?ref=master -O scripts.zip
unzip scripts.zip

# Clean up
mv install-scripts* scripts/
rm scripts.zip
```

Each script runs a silent installation for the product it represents.  To install, 
use the command: `sh <install script name>`

For example, to install Java:

```
cd scripts
sh java.sh
```
**BTW**: If you are installing anything that depends on Java, install Java first.
