sudo yum install tomcat7 tomcat7-webapps tomcat7-docs-webapp tomcat7-admin-webapps -y
sudo chkconfig tomcat7 on
sudo service tomcat7 start

echo === Tomcat Directories ===
echo /etc/tomcat7      - configuration files
echo /var/log/tomcat7  - log files
echo You must change admin password here: /etc/tomcat7/tomcat-user.xml
echo 

echo ==== Useful Commands ===
echo sudo service tomcat7 start
echo sudo service tomcat7 stop
echo sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j DNAT --to-destination :8080