# Create the user
sudo adduser h2

# create the log location
sudo mkdir /var/log/h2
sudo chown h2:h2 /var/log/h2

# In a browser, go to http://www.h2database.com/html/download.html.  Find the Last Stable version.   Right click on 
# "Platform-Independent Zip", select "Copy Link Address" and download with wget like this:
sudo wget http://www.h2database.com/h2-2017-03-10.zip -O h2.zip

sudo unzip h2.zip
sudo mv h2 /opt/h2
sudo chown h2:h2 /opt/h2
sudo rm h2.zip

# Install startup script
sudo cp ./resources/h2 /etc/init.d/h2
sudo chmod 755 /etc/init.d/h2
sudo chkconfig --add /etc/init.d/h2

# Start the service
sudo service h2 init

# Show output from startup
sudo cat /var/log/h2/h2.log 

