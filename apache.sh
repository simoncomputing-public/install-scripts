#Install Apache
sudo yum install -y httpd24

# Set permissions for the /var/www/ folder
sudo groupadd www
sudo usermod -a -G www ec2-user

# Set the /var/www directory
sudo chown -R root:www /var/www
sudo chmod 2775 /var/www
find /var/www -type d -exec sudo chmod 2775 {} \;
find /var/www -type f -exec sudo chmod 0664 {} \;

#Set to start on reboot
sudo chkconfig httpd on

#Start Apache
sudo service httpd start

echo === File Locations ===
echo /var/www  contains the web files
echo /etc/httpd/conf/httpd.conf   configuration file
echo /var/log/httpd  contains log files

echo === Useful Commands ===
echo sudo service httpd start
echo sudo service httpd stop
echo sudo chkconfig httpd on


